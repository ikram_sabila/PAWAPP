<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_Kegiatan', function (Blueprint $table) {
            $table->char('ID_Kegiatan', 5)->primary();
            $table->string('Kategori_Kegiatan', 30);
            $table->string('Nama_Kegiatan', 50);
            $table->string('Foto_Kegiatan', 100);
            $table->date('Tanggal_Kegiatan');
            $table->string('Link_Kegiatan', 100);
            // Tambahkan kolom lainnya sesuai kebutuhan
            // $table->timestamps(); // Jika ingin menggunakan created_at dan updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_Kegiatan');
    }
};