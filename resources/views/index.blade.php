<!-- resources/views/home/show_photo_and_name.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>

    <h1>List of Activities</h1>

    @foreach ($kegiatans as $kegiatan)
        <div>
            <h2>{{ $kegiatan->Nama_Kegiatan }}</h2>
            <img src="{{ asset('storage/' . $kegiatan->Foto_Kegiatan) }}" alt="{{ $kegiatan->Nama_Kegiatan }}" width="300">
        </div>
    @endforeach

</body>
</html>