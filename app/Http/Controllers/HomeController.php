<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Mengambil semua data kegiatan dari database
        $kegiatans = Home::all();

        // Menampilkan tampilan index dan mengirimkan data kegiatan
        return view('index', compact('kegiatans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('home.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Kategori_Kegiatan' => 'required',
            'Nama_Kegiatan' => 'required',
            'Foto_Kegiatan' => 'required',
            'Tanggal_Kegiatan' => 'required|date',
            'Link_Kegiatan' => 'required',
        ]);

        Home::create($request->all());

        return redirect()->route('home.index')
            ->with('success', 'Kegiatan created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function show(Home $home)
    {
        return view('home.show', compact('home'));
    }

    // Metode-metode lainnya sesuai kebutuhan
}
